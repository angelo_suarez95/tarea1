<html>
  <head>
    <meta charset="utf-8">
    <style>
      table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
      }
      th, td {
        padding: 5px;
        text-align: left;
      }
      #t1{
        background-color:#d0d9d5;
     }
      #t2{
        background-color:#9de0c0;
     }
    </style>
  </head>
  <body>
  <?php
   echo "<table align='center'>";
    echo "  <tr>";
    echo "    <th colspan='3' style='background-color:yellow'><p align='center'>Productos</p></th>";
    echo "  </tr>";
    echo "  <tr id='t1'>";
    echo "    <td> <p align='center'>Nombre</p></td>";
    echo "    <td> <p align='center'> Cantidad</p></td>";
    echo "    <td> <p align='center'> Precio (Gs)</p></td>";
    echo "  </tr>";
    echo "  <tr>";
    echo "    <td>Coca-Cola</td>";
    echo "    <td><p align='center'>100</p></td>";
    echo "    <td><p align='center'>4.500</p></td>";
    echo "  </tr>";
    echo "  <tr id='t2'>";
    echo "    <td>Pepsi</td>";
    echo "    <td><p align='center'>30</p></td>";
    echo "    <td><p align='center'>4.800</p></td>";
    echo "  </tr>";
    echo "  <tr>";
    echo "   <td>Sprite</td>";
    echo "    <td><p align='center'>20</p></td>";
    echo "    <td><p align='center'>4.500</p></td>";
    echo "  </tr>";
    echo "  <tr id='t2'>";
    echo "    <td>Guarana</td>";
    echo "    <td><p align='center'>200</p></td>";
    echo "    <td><p align='center'>4.500</p></td>";
    echo "  </tr>";
    echo "  <tr>";
    echo "    <td>SevenUp</td>";
    echo "    <td><p align='center'>24</p></td>";
    echo "    <td><p align='center'>4.800</p></td>";
    echo "  </tr>";
    echo "  <tr id='t2'>";
    echo "    <td>Mirinda Naranja</td>";
    echo "    <td><p align='center'>56</p></td>";
    echo "    <td><p align='center'>4.800</p></td>";
    echo "  </tr>";
    echo "  <tr>";
    echo "    <td>Mirinda Guarana</td>";
    echo "    <td><p align='center'>89</p></td>";
    echo "    <td><p align='center'>4.800</p></td>";
    echo "  </tr>";
    echo "  <tr id='t2'>";
    echo "    <td>Fanta naranja</td>";
    echo "    <td><p align='center'>10</p></td>";
    echo "    <td><p align='center'>4.500</p></td>";
    echo "  </tr>";
    echo "  <tr>";
    echo "    <td>Fanta Piña</td>";
    echo "    <td><p align='center'>2</p></td>";
    echo "   <td><p align='center'>4.500</p></td>";
    echo " </tr>";
    echo "</table>";
 ?>
  </body>
</html>